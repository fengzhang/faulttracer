package tracer.callgraph.test.instr;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

@Deprecated
public class InstruClassAdapter extends ClassVisitor {
	String name;

	public InstruClassAdapter(int api, ClassVisitor cv) {
		super(api, cv);
	}

	@Override
	public void visit(final int version, final int access, final String name,
			final String signature, final String superName,
			final String[] interfaces) {
		this.name = name;
		cv.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc,
			String signature, String[] exceptions) {
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature,
				exceptions);
//		 if (name.startsWith("test") || name.startsWith("compare")
//		 || name.startsWith("check"))
if (name.startsWith("test"))
			return new InstruTestMethodAdapter(Opcodes.ASM5, mv, this.name + ":" + name + ":"
					+ desc);
		else
			return mv;
//			return new InstruMethodAdapter(mv, access, this.name + ":" + name
//					+ ":" + desc);
	}

}
