package tracer.coverage.core;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

import tracer.coverage.junit.TestDetector;

public class CoverageClassAdapter extends ClassVisitor {
	String name;

	public CoverageClassAdapter(int api, ClassVisitor cv) {
		super(api, cv);
	}

	@Override
	public void visit(final int version, final int access, final String name,
			final String signature, final String superName,
			final String[] interfaces) {
		this.name = name;
		cv.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc,
			String signature, String[] exceptions) {
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature,
				exceptions);
		// if(TestDetector.isTestMethod(name))
		// return new CoverageTestMethodAdapter(Opcodes.ASM5, mv, access,this.name+":"+name +
		// ":" + desc);
		if (Properties.METHOD_COV)
			return new MethodCoverageMethodAdapter(Opcodes.ASM5, mv, access, this.name + ":"
					+ name + ":" + desc);
		else if (Properties.STATEMENT_COV)
			return new StatementCoverageMethodAdapter(Opcodes.ASM5, mv, access, this.name
					+ ":" + name + ":" + desc);
		else if (Properties.BRANCH_COV)
			return new BranchCoverageMethodAdapter(Opcodes.ASM5, mv, access, this.name + ":"
					+ name + ":" + desc);
		return new ECGCoverageMethodAdapter(Opcodes.ASM5, mv, access, this.name + ":" + name
				+ ":" + desc);
	}

}
