package tracer.coverage.junit;


import static org.objectweb.asm.Opcodes.*;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class TransformTestSuiteMethodAdapter extends MethodVisitor {

	private static Logger logger = LogManager
			.getLogger(TransformTestSuiteMethodAdapter.class);

	private String targetClass;

	private String transformMethod;

	private String transformMethodSignature;

	public TransformTestSuiteMethodAdapter(int api, MethodVisitor mv,
			String targetClass, String integrationMethod,
			String integrationMethodSignature) {
		super(api, mv);
		this.targetClass = targetClass;
		this.transformMethod = integrationMethod;
		this.transformMethodSignature = integrationMethodSignature;
	}

	@Override
	public void visitInsn(int opcode) {
		if (opcode == Opcodes.ARETURN) {
			logger.info("Transforming Testsuite+ " + targetClass  + "    "  + transformMethod);
			mv.visitMethodInsn(INVOKESTATIC, targetClass, transformMethod,
					transformMethodSignature);

		}

		mv.visitInsn(opcode);
	}

}
